package Archive::BagIt::Plugin::Manifest::SHA256;
# ABSTRACT: The role to load the sha256 plugin
# VERSION

use strict;
use warnings;
use Moo;
with 'Archive::BagIt::Role::Manifest';

has '+plugin_name' => (
    is => 'ro',
    default => 'Archive::BagIt::Plugin::Manifest::SHA256',
);

has 'manifest_path' => (
    is => 'ro',
);

has 'manifest_files' => (
    is => 'ro',
);

has '+algorithm' => (
    is => 'rw',
);

sub BUILD {
    my ($self) = @_;
    $self->bagit->load_plugins(("Archive::BagIt::Plugin::Algorithm::SHA256"));
    $self->algorithm($self->bagit->plugins->{"Archive::BagIt::Plugin::Algorithm::SHA256"});
    return 1;
}

1;
