package Archive::BagIt::Plugin::Algorithm::SHA256;
use strict;
use warnings;
use Carp qw( croak );
use Moo;
use namespace::autoclean;
with 'Archive::BagIt::Role::Algorithm';
with 'Archive::BagIt::Role::OpenSSL';
# VERSION
# ABSTRACT: The SHA256 algorithm plugin

has '+plugin_name' => (
    is => 'ro',
    default => 'Archive::BagIt::Plugin::Algorithm::SHA256',
);

has '+name' => (
    is      => 'ro',
    #isa     => 'Str',
    default => 'sha256',
);

__PACKAGE__->meta->make_immutable;
1;
